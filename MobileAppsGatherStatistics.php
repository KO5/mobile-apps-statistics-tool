<?php

namespace Console\Commands;

use Package\Analytics\MobileApps\MobileAppAnalytics;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Package\Analytics\MobileApps\PlayMarketAppsAnalytics;
use Package\Analytics\MobileApps\FacebookAppsAnalytics;

class MobileAppsGatherStatistics extends Command
{

    protected $title = 'Mobile apps statistics retrieving interface';

    protected function configure()
    {
        $this
            ->setName('product:mobileapps-statistics')
            ->setDescription($this->title)
            ->setHelp($this->title);

        $this
            ->addOption('count-of-days', 'c', InputOption::VALUE_OPTIONAL, 'Number of days to retrieve')
            ->addOption('date-from', 'd', InputOption::VALUE_OPTIONAL, 'Starting from what date?');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * Here you can set credentials for app, for which you want gather statistics
         */
        $appsMap = [

            //Data set for retrieving PDFfiller iOS app
            'pdffillerIos' => [
                //Class which should handle data
                'className' => FacebookAppsAnalytics::class,
                //Metrics parameter, where key means field in our database,
                //and the value - associated column from file
                'metrics' => [
                    'installs' => 'fb_mobile_first_app_launch'
                ],
                //apiId - Application id from our system
                'apiId' => PDFFILLER_IOS_API_ID,
                'options' => [
                    //apiId - Application id from 3rd-party system
                    'applicationId' => FACEBOOK_PDFFILLER_IOS_APP_ID
                ]
            ],

            //Data set for retrieving PDFfiller Android app
            'pdffillerAndroid' => [
                //Class which should handle data
                'className' => PlayMarketAppsAnalytics::class,
                //Metrics parameter, where key means field in our database,
                //and the value - associated column from file
                'metrics' => [
                    'installs' => 'Daily Device Installs'
                ],
                //apiId - Application id from our system
                'apiId' => PDFFILLER_ANDROID_API_ID,
                'options' => [
                    //apiId - Application id from 3rd-party system
                    'packageName' => 'pdffiller',
                    //type of report from Google Console
                    'reportType' => 'overview',
                ]
            ]
        ];

        if ($countOfDays = $input->getOption('count-of-days')) {
            if ((int)$countOfDays === 0) {
                $output->writeln('--count-of-days must be an integer and not equal to 0');
                return false;
            }
        }
        if ($dateFrom = $input->getOption('date-from')) {
            if (!MobileAppAnalytics::validDateFormat($dateFrom)) {
                $output->writeln('--date-from option value should be in following format: Y-m-d');
                return false;
            }
        }

        foreach ($appsMap as $key => $app) {
            $className = $app['className'];
            $appAnalyticsClass = (new $className(
                $app['metrics'],
                $app['apiId'],
                $app['options']
            ));

            if ($countOfDays) {
                $appAnalyticsClass->setRecordingStepSize($countOfDays);
            }
            if ($dateFrom) {
                $appAnalyticsClass->setEndDate($dateFrom);
            }

            if (!$appAnalyticsClass->retrieveData()) {
                $output->writeln($className.' was crashed on retrieving data');
                continue;
            }
            if (!$appAnalyticsClass->prepareData()) {
                $output->writeln($className.' was crashed on preparing data');
                continue;
            }
            if (!$appAnalyticsClass->saveData()) {
                $output->writeln($className.' was crashed on saving data');
                continue;
            }
            $output->writeln('Operation was finished successfully for '.$key);
        }
    }
}
