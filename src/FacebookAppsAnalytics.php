<?php

namespace Package\Analytics\MobileApps;

use Exception;
use GuzzleHttp;

class FacebookAppsAnalytics extends MobileAppAnalytics
{
    /**
     * @var integer
     */
    private $applicationId;

    /**
     * @var string
     */
    private $response;

    /**
     * @var array
     */
    private $allowedEventTypes = [
        'fb_mobile_first_app_launch',
        'fb_mobile_activate_app',
        'fb_mobile_complete_registration',
        'fb_mobile_login_fas_dialog_result'
    ];

    const FACEBOOK_GRAPH_API_URL = 'https://graph.facebook.com/';
    const FACEBOOK_GRAPH_API_VERSION = 'v2.7';

    /**
     * FacebookAppsAnalytics constructor.
     * @param $metrics
     * @param $apiId
     * @param $options
     * @throws MobileAppsException
     */
    public function __construct($metrics, $apiId, $options)
    {
        foreach ($metrics as $event) {
            if (!in_array($event, $this->allowedEventTypes)) {
                throw new MobileAppsException(
                    'Incorrect event type was passed to ' . self::class . ' constructor. See $allowedEventTypes.'
                );
            }
        }
        if (empty($options['applicationId'])) {
            throw new MobileAppsException(
                '$options array should contain applicationId. Can be found in facebook developer console'
            );
        }
        $this->applicationId = $options['applicationId'];
        parent::__construct($metrics, $apiId);
    }

    /**
     * @return bool
     * @throws MobileAppsException
     * At that point we should have several set's of data divided by days
     * $this->data['installs'], $this->data['registrations'], etc.
     * We must union several data sets in one data set with several metrics
     */
    public function prepareData()
    {
        $preparedData = [];
        /**
         * Walk though each data set, where key of data set equals to metric field,
         * passed to constructor, eg 'installs', 'registrations', etc.
         */
        foreach ($this->metrics as $field => $event) {
            /**
             * Walk though each item in data set.
             */
            foreach ($this->data[$field] as $dataItem) {
                $dataItemTime = date('Y-m-d', strtotime($dataItem->time));
                /**
                 * We can union metrics in one data set using unic key - date of the day.
                 */
                $dataItemKey = $dataItemTime;
                if (!isset($preparedData[$dataItemKey])) {
                    $preparedData[$dataItemKey] = [
                        'date' => $dataItemTime,
                    ];
                }
                /**
                 * Add metric to current day item in united data set.
                 */
                $preparedData[$dataItemKey][$field] = $dataItem->value;
            }
        }
        $this->data = $preparedData;
        if (!$this->validateData()) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function retrieveData()
    {
        try {
            foreach ($this->metrics as $field => $event) {
                $request = $this->prepareRequest($event);
                $this->makeHttpRequest($request);
                $results = json_decode($this->response);
                if ($results === false) {
                    throw new MobileAppsException('Error parsing JSON from response');
                }
                $this->data[$field] = $results->data;
            }
        } catch (MobileAppsException $e) {
            logger('mobile_apps')->alert(
                'Error has occurred while retrieving data',
                array_merge($e->getContext(), ['object' => get_object_vars($this)])
            );
            return false;
        } catch (Exception $e) {
            logger('mobile_apps')->exception($e);
            return false;
        }
        return true;
    }

    /**
     * @param $event
     * @return string
     * @throws MobileAppsException
     */
    public function prepareRequest($event)
    {
        $this->setPeriodDates();
        if (empty($this->applicationId) ||
            empty($this->periodDates)
        ) {
            throw new MobileAppsException('Not enough required params to generate Request URL');
        }
        $query = http_build_query([
            'since' => strtotime($this->periodDates['startDate'] . '+1 day'),
            'until' => strtotime($this->periodDates['endDate'] . '+1 day'),
            'summary' => 'true',
            'event_name' => $event,
            'aggregateBy' =>'COUNT',
            'access_token' => $this->applicationId. '|' .FACEBOOK_API_SECRET
        ]);
        $path = implode('/', [
            self::FACEBOOK_GRAPH_API_VERSION,
            $this->applicationId,
            'app_insights',
            'app_event'
        ]);
        return self::FACEBOOK_GRAPH_API_URL . $path . '?' . $query;
    }

    /**
     * @param $url
     * @return bool
     * @throws MobileAppsException
     */
    private function makeHttpRequest($url)
    {
        $client = new GuzzleHttp\Client();
        $response = $client->request('GET', $url);
        $response = (string)$response->getBody();
        if (!$this->response = $response) {
            throw new MobileAppsException('Could not get body from response with following url -'.$url);
        }
        return true;
    }
}
