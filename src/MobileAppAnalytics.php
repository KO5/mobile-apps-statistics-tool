<?php

namespace Package\Analytics\MobileApps;

use Eloquent\MobileApplicationAnalytics;

abstract class MobileAppAnalytics
{
    /**
     * Writing statistics data only for previous day is dangerous,
     * because we gather data from 3rd-party services like Facebook, Google, etc.
     * In case halting on their servers, we may skip days.
     * To be safely, we must retrieve and write to DB statistics data for the past 5-10 days.
     * $recordingStepSize means number of days, which we retrieve and write to DB by one procedure call.
     */
    protected $recordingStepSize = 7;

    /**
     * @var int
     * ApiId helps us to associate metrics with app in our system
     */
    protected $apiId;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     * Should be associative array, where keys means fields
     * in mobile_apps_analytics table, and the values is
     * corresponding metric name in API service
     */
    protected $metrics = [];

    /**
     * @var string
     * End date of period, for which we should gather statistics
     * (including this day)
     * Format: 'Y-m-d'
     */
    protected $endDate;

    /**
     * @var array
     */
    protected $periodDates;

    /**
     * MobileAppAnalytics constructor.
     * @param $metrics
     * @param $apiId
     * @throws MobileAppsException
     */
    public function __construct($metrics, $apiId)
    {
        if (empty($apiId) ||
            empty($metrics)
        ) {
            throw new MobileAppsException(
                self::class . ' can\'t be used without required parameters. See class constructor.'
            );
        }

        /**
         * $metrics should be associative array, where keys means fields
         * in mobile_apps_analytics table, and the values is
         * corresponding column name from CSV file.
         */
        if (!is_array($metrics) || array_values($metrics) === $metrics) {
            throw new MobileAppsException(
                '$metrics should be associative array. See class constructor for details.'
            );
        }

        if (count(
            array_intersect(
                array_keys($metrics),
                (new MobileApplicationAnalytics)->fillable
            )
        ) !==
            count(
                array_keys($metrics)
            )
        ) {
            throw new MobileAppsException(
                'One of metric fields not marked as fillable in ' .
                MobileApplicationAnalytics::class . ' eloquent model'
            );
        }

        $this->metrics = $metrics;
        $this->apiId = $apiId;
    }

    /**
     * @param $size
     */
    public function setRecordingStepSize($size)
    {
        $this->recordingStepSize = $size;
    }

    /**
     * @param $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @param $date
     * @param string $format
     * @return bool
     */
    public static function validDateFormat($date, $format = 'Y-m-d')
    {
        return date($format, strtotime($date)) == $date;
    }

    /**
     * @return bool
     * @throws MobileAppsException
     */
    protected function setPeriodDates()
    {

        if (empty($this->endDate)) {
            $this->endDate = date('Y-m-d');
        } else {
            if (!$this->validDateFormat($this->endDate)) {
                throw new MobileAppsException('Invalid endDate format. Use "Y-m-d" format');
            }
        }

        $this->periodDates['endDate'] = $this->endDate;
        $this->periodDates['startDate'] = date(
            'Y-m-d',
            /**
             * We includes endDate day, that's why we should remove one day from start of period
             */
            strtotime('-'.($this->recordingStepSize-1).' days', strtotime($this->endDate))
        );

        return true;
    }
    /**
     * @return bool
     * @throws MobileAppsException
     */
    protected function validateData()
    {
        if (!is_array($this->data) || empty($this->data)) {
            throw new MobileAppsException('Data is empty or not an array');
        }
        foreach ($this->data as $subData) {
            /**
             * subData must contain at least two items:
             * Date of the day, and metric for this day, e.g. Installs, Uninstalls, Registrations, etc.
             */
            if (!is_array($subData)) {
                throw new MobileAppsException('Data item not an array');
            }
            if (count($subData) < 2) {
                throw new MobileAppsException('Data item have not enough elements');
            }
            /**
             * Date of the day must be in 'Y-m-d' format.
             */
            if (!isset($subData['date'])) {
                throw new MobileAppsException('Column date are missing');
            }
            if (!$this->validDateFormat($subData['date'])) {
                throw new MobileAppsException('Date have wrong format');
            }
        }
        return true;
    }

    /**
     * Here should be API calls
     * @return array
     */
    abstract public function retrieveData();

    /**
     * Should prepare mixed, raw data for writing to DB.
     * @return array
     */
    abstract public function prepareData();

    /**
     * Can use MobileAppInstallsValidator to validate data before saving.
     * @return boolean
     * @throws MobileAppsException
     */
    public function saveData()
    {
        try {
            if (!$this->validateData()) {
                throw new MobileAppsException('Invalid data can not be saved');
            }
            foreach ($this->data as $subData) {
                $attributes = [];
                foreach ($this->metrics as $field => $metric) {
                    $attributes[$field] = $subData[$field];
                }
                if (empty($attributes)) {
                    throw new MobileAppsException('Empty attributes in dataItem while saving');
                }
                MobileApplicationAnalytics::updateOrCreate([
                    'date' => $subData['date'],
                    'api_id' => $this->apiId
                ], $attributes);
            }
        } catch (MobileAppsException $e) {
            logger('mobile_apps')->alert(
                'Error has occurred while saving data',
                array_merge($e->getContext(), ['object' => get_object_vars($this)])
            );
            return false;
        } catch (\Exception $e) {
            logger('mobile_apps')->exception($e);
            return false;
        }
        return true;
    }
}
