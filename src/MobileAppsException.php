<?php

namespace Package\Analytics\MobileApps;

use Exception;

class MobileAppsException extends Exception
{

    public function getContext()
    {
        return [
            'file' => $this->file,
            'message' => $this->message,
            'line' => $this->line,
        ];
    }
}
