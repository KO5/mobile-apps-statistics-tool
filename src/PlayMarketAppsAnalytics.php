<?php

namespace Package\Analytics\MobileApps;

use Google_Client;
use Google_Service_Storage;
use GuzzleHttp;
use DateTime;
use DateInterval;
use DatePeriod;

/**
 * Class PlayMarketAppsAnalytics
 * @package Package\Analytics\MobileApps
 * Google gathers mobile apps statistics in CSV files.
 * CSV files can be downloaded from Google Cloud Storage (GCS)
 * You can download CSV file by one certain month.
 */
class PlayMarketAppsAnalytics extends MobileAppAnalytics
{
    /**
     * @var string
     * Needed to generate filename.
     * Can be found in google play developer console.
     */
    protected $packageName;

    /**
     * @var string
     * Type of report, should be one of defined in $allowedReportTypes
     */
    protected $reportType = '';

    /**
     * @var array
     */
    private $allowedReportTypes = [
       'tablet',
        'overview',
        'os_version',
        'language',
        'device',
        'country',
        'carrier',
        'app_version'
    ];

    /**
     * @var array
     */
    private $columnPositions;

    /**
     * @var Google_Client
     */
    private $client;

    /**
     * @var Google_Service_Storage
     */
    private $storage;

    /**
     * PlayMarketAppsAnalytics constructor.
     * @param $apiId
     * @param $metrics
     * @param $options
     * @throws MobileAppsException
     * See comments for required properties
     */
    public function __construct($metrics, $apiId, $options)
    {
        if (empty($options['packageName']) ||
            empty($options['reportType'])
        ) {
            throw new MobileAppsException(
                self::class.' can\'t be used without required parameters. See class constructor.'
            );
        }
        if (!in_array($options['reportType'], $this->allowedReportTypes)) {
            throw new MobileAppsException(
                'Incorrect report type was passed to ' . self::class . ' constructor. See $allowedReportTypes.'
            );
        }
        $this->packageName = $options['packageName'];
        $this->reportType = $options['reportType'];
        parent::__construct($metrics, $apiId);
    }

    /**
     * @return bool
     */
    public function retrieveData()
    {
        try {
            $this->setPeriodDates();
            $months = $this->getMonthsFromPeriod();
            $fileNames = $this->getFileNames($months);
            $mediaLinks = $this->getMediaLinks($fileNames);
            $data = [];
            foreach ($mediaLinks as $mediaLink) {
                $file = $this->downloadCsvFile($mediaLink);
                $data = array_merge($data, $this->parseCsvFile($file));
            }
        } catch (\Google_Service_Exception $e) {
            logger('mobile_apps')->exception($e);
            return false;
        } catch (MobileAppsException $e) {
            logger('mobile_apps')->alert(
                'Error has occurred while retrieving data',
                array_merge($e->getContext(), ['object' => get_object_vars($this)])
            );
            return false;
        } catch (\Exception $e) {
            logger('mobile_apps')->exception($e);
            return false;
        }
        if (empty($data)) {
            return false;
        }
        $this->data = $data;
        return true;
    }

    /**
     * @return bool
     */
    public function prepareData()
    {
        if (!$rawData = $this->data) {
            return false;
        }
        try {
            $this->setColumnPositions($this->data);

            $preparedData = [];
            foreach ($this->data as $rawDataRow) {
                if ($this->validateDataItem($rawDataRow)) {
                    $results = [];
                    foreach ($this->columnPositions as $field => $position) {
                        $results[$field] = $rawDataRow[$position];
                    }
                    $preparedData[] = $results;
                }
            }
        } catch (MobileAppsException $e) {
            logger('mobile_apps')->alert(
                'Error has occurred while preparing data',
                array_merge($e->getContext(), ['object' => get_object_vars($this)])
            );
            return false;
        }
        if ($this->data == $preparedData) {
            return false;
        }
        $this->data = $preparedData;
        return true;
    }

    /**
     * @return bool
     */
    private function setClient()
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.GOOGLE_SERVICE_ACCOUNT_KEY_FILE);
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->addScope(Google_Service_Storage::DEVSTORAGE_FULL_CONTROL);
        if (!$this->client = $client) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    private function setStorage()
    {
        if (!$this->client) {
            if (!$this->setClient()) {
                return false;
            }
        }
        $storage = new Google_Service_Storage($this->client);
        $this->storage = $storage;
        return true;
    }

    /**
     * @return array
     * @throws MobileAppsException
     * We can download one file per month, that's why we
     * should generate dates for every month in period
     */
    public function getMonthsFromPeriod()
    {
        $months = [];
        $start    = (new DateTime($this->periodDates['startDate']))->modify('first day of this month');
        $end      = (new DateTime($this->periodDates['endDate']))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $months[]= $dt->format("Ym");
        }

        return $months;
    }

    /**
     * @param $dates
     * @return array
     * @throws MobileAppsException
     * Filename of CSV is consists from several parts:
     * Application name, date and type of report.
     */
    private function getFileNames($dates)
    {
        $fileNames = [];
        foreach ($dates as $date) {
            $fileNames[] = 'installs_com.'.$this->packageName.'_'.$date.'_'.$this->reportType.'.csv';
        }
        if (empty($fileNames)) {
            throw new MobileAppsException('Could not generate file names');
        }
        return $fileNames;
    }

    /**
     * @param $fileNames
     * @return array
     * @throws MobileAppsException
     * Since we know file names, we can try to download it.
     * At first, we must get mediaLinks for this files.
     */
    private function getMediaLinks($fileNames)
    {
        $mediaLinks = [];
        if (!$this->storage) {
            if (!$this->setStorage()) {
                throw new MobileAppsException('Can not set GCS');
            }
        }
        foreach ($fileNames as $fileName) {
            try {
                $csvFile = $this->
                storage->
                objects->
                get(GOOGLE_CLOUD_STORAGE_STATISTICS_BUCKET, 'stats/installs/'.$fileName);
            } catch (\Google_Service_Exception $e) {
                $exception = $e->getErrors();
                /** In case of exception, check error message */
                if (isset($exception[0]['message']) &&
                    /** If GCS says, that file is not found, we should add alert to LogSystem ... */
                    $exception[0]['message'] == 'Not Found' &&
                    /**
                     * ... but not always.
                     * At first days of month, CSV file for this month may be not generated yet,
                     * so we should'nt create fake alerts
                     */
                    (int) date('d') > 3
                ) {
                    throw new MobileAppsException('File '.$fileName.' not found on server');
                    /** Other ways, write whole Exception to LogSystem ...  */
                } else {
                    logger('mobile_apps')->exception($e, ['filename' => $fileName]);
                }
                /** ... and skip current iteration */
                continue;
            }
            $mediaLinks[] = $csvFile['mediaLink'];
        }
        return $mediaLinks;
    }

    /**
     * @param $mediaLink
     * @return string
     * @throws MobileAppsException
     * Using Media Links, we able to make authorized HTTP calls
     * and get content of CSV files
     */
    private function downloadCsvFile($mediaLink)
    {
        $request = new GuzzleHttp\Psr7\Request('GET', $mediaLink);
        $file = $this->client->authorize()->send($request)->getBody()->getContents();
        if (empty($file)) {
            throw new MobileAppsException('Could not make authorized http request to media link');
        }
        return $file;
    }

    /**
     * @param $file
     * @return array
     * @throws MobileAppsException
     */
    private function parseCsvFile($file)
    {
        $data = [];
        $file = iconv('UTF-16', 'UTF-8', $file);
        $lines = explode(PHP_EOL, $file);
        foreach ($lines as $line) {
            $data[] = str_getcsv($line);
        }
        if (empty($data)) {
            throw new MobileAppsException('Empty data after file parsing');
        }
        return $data;
    }

    private function setColumnPositions($rawData)
    {
        /**
         * Date - default column in any report file, we can hardcode it.
         * First element of $rawData is always table heading
         * Otherwise - something went wrong
         */
        $dateColumnPos = array_search('Date', $rawData[0]);
        if ($dateColumnPos === false) {
            throw new MobileAppsException(
                'Column Date does not exists in CSV file, probably something wrong with file'
            );
        }
        $this->columnPositions = [
            'date' => $dateColumnPos
        ];

        /**
         * To walk though each row and gather stats, we should know
         * positions of required columns, that was passed to constructor
         */
        foreach ($this->metrics as $field => $columnName) {
            $metricPos = array_search($columnName, $rawData[0]);
            if ($metricPos === false) {
                throw new MobileAppsException('Column '.$columnName.' does not exists in CSV file');
            }
            $this->columnPositions[$field] = $metricPos;
        }
        return true;
    }

    private function validateDataItem($dataItem)
    {
        /**
         * Check to see if the values exists for the specified columns in current row
         */
        foreach ($this->columnPositions as $field => $pos) {
            if (empty($dataItem[$pos])) {
                return false;
            }
        }

        $dataItemDateField = $dataItem[$this->columnPositions['date']];
        $timeItem = strtotime($dataItemDateField);
        $timeEnd = strtotime($this->periodDates['endDate']);
        $timeStart = strtotime($this->periodDates['startDate']);

        return $this->validDateFormat($dataItemDateField) &&
        /**
         * We include item, only if his date in $this->periodDates range
         */
        $timeItem <= $timeEnd &&
        $timeItem >= $timeStart;
    }
}
